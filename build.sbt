name := "esperman"
version := "0.0.1"

com.twitter.scrooge.ScroogeSBT.newSettings

libraryDependencies += "org.apache.thrift" % "libthrift" % "0.9.2"
libraryDependencies += "com.twitter" % "scrooge-core_2.10" % "3.16.3"
libraryDependencies += "com.twitter" % "finagle-thrift_2.10" % "6.24.0"
libraryDependencies += "com.twitter" % "twitter-server_2.10" % "1.9.0"
libraryDependencies += "com.espertech" % "esper" % "5.1.0"

