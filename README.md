esperman - esper example using thrift-finagle
================

## Server

*Compile*
```
sbt compile
```

*Run*
```
sbt run
```


## Client

*Compile*
```
./thrift_compile_for_py_client.sh
```

*Run*
```
./src/main/python_client/run_client.py
```

