namespace java org.zwolf.esperman
#@namespace scala org.zwolf.esperman

struct DateTime {
	1: i16 year,
	2: byte month,
	3: byte day,
	4: byte hour,
	5: byte minute,
	6: byte second 
}

struct TestEventData {
	1: DateTime logTime,
	2: string ip,
	3: i64 logSeq,
	4: byte resultNumber,
	5: string text,
	6: i32 pcCount,
	7: byte logNumber
}

service LogEvent {
	void sendTestEventData(1: TestEventData result)
}

