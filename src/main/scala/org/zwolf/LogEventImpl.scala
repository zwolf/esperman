package org.zwolf.esperman

import com.twitter.logging.Logger
import com.twitter.util.Future
import scala.collection.mutable
import com.twitter.conversions.time._
import com.twitter.finagle.util.{TimerFromNettyTimer => FinagleTimer}

import com.espertech.esper.client.EPServiceProvider

import org.zwolf.esperman._


class LogEventImpl() extends LogEvent.FutureIface {
  val log = Logger.get(getClass)
  var epService: EPServiceProvider = _;

  def setEPServiceProvider(epServiceProvider: EPServiceProvider) = {
    epService = epServiceProvider;
  }

  def sendTestEventData(result: TestEventData) = {
/*
    log.info("TestEventData: DateTime: %s-%s-%s, logSeq: %s", 
      result.logTime.year.toString,
      result.logTime.month.toString,
      result.logTime.day.toString,
      result.logSeq.toString)
*/
    epService.getEPRuntime().sendEvent(result);
    Future.Unit
  }

}
