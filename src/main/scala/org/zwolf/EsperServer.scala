package org.zwolf.esperman

import java.net.InetSocketAddress
import com.twitter.finagle.builder.{Server, ServerBuilder}
import com.twitter.finagle.thrift.ThriftServerFramedCodec
import com.twitter.server.TwitterServer
import com.twitter.util.Await
import com.twitter.finagle.stats._
import org.apache.thrift.protocol.TBinaryProtocol

import com.espertech.esper.client.Configuration
import com.espertech.esper.client.ConfigurationEventTypeLegacy
import com.espertech.esper.client.EPServiceProvider
import com.espertech.esper.client.EPServiceProviderManager
import com.espertech.esper.client.UpdateListener
import com.espertech.esper.client.EventBean

import org.zwolf.esperman._


object EsperServer extends TwitterServer {
  val serviceAddr = flag("service.address", new InetSocketAddress("localhost", 8889), "Service Port")
  val serviceName = flag("service.name", "EsperServer", "EsperServer using Thrift-Finagle")

  protected def listner1() = new UpdateListener {
    override def update(newEvents: Array[EventBean], oldEvents: Array[EventBean]) {
      if (newEvents != null) {
        print("listenr1: ")
        newEvents.foreach { ev =>
          println(ev.getUnderlying())
        }
      }
    }
  }

  protected def listner2() = new UpdateListener {
    override def update(newEvents: Array[EventBean], oldEvents: Array[EventBean]) {
      if (newEvents != null) {
        println("listenr2: ")
        newEvents.foreach { ev =>
          println("\t" + ev.getUnderlying())
        }
      }
    }
  }

  def main() = {
    // epser init.
    val config = new Configuration

    // set event public accessor for thrift object
    val legacyDef = new ConfigurationEventTypeLegacy();
    legacyDef.setAccessorStyle(ConfigurationEventTypeLegacy.AccessorStyle.PUBLIC);

    // add event type
    config.addEventType("TestEventData", classOf[TestEventData].getName, legacyDef)

    val epService = EPServiceProviderManager.getProvider(this.getClass.getName, config)
    epService.initialize

    // add epl & listeners
    val statment1 = epService.getEPAdministrator().createEPL("""
        select * from TestEventData
        """)
    statment1.addListener(listner1)

    val statment2 = epService.getEPAdministrator().createEPL("""
        select resultNumber, count(1) as count, min(logSeq) as minLogSeq, max(logSeq) as maxLogSeq, avg(logNumber) as avgLogNumber 
        from TestEventData.win:length_batch(5) 
        group by resultNumber
        """)
    statment2.addListener(listner2)

    // run thrift-finagle service
    val processor = new LogEventImpl()
    processor.setEPServiceProvider(epService) // set esper service provider

    val service = new LogEvent.FinagledService(processor, new TBinaryProtocol.Factory)

    val server: Server = ServerBuilder()
      .codec(ThriftServerFramedCodec())
      .bindTo(serviceAddr())
      .name(serviceName())
      .build(service)
    onExit {
      service.close()
    }

    Await.ready(adminHttpServer)
  }
}
